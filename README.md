# Puma Prey edit  
Puma Prey contains vulnerable .NET applications, which provide a target for secure coding challenges, CTFs, and testing the Puma Scan analyzers.

This project contains Web Forms, MVC5, and Core web applications that communicate with web services and data layer applications.

## Projects

### Coyote
ASP.NET Core / Identity web app for testing the Puma Scan rules engine.

### Raccoon
ASP.NET 4.6 MVC 5 / Identity web app for testing the Puma Scan rules engine.

### Skunk
ASP.NET 4.6 Web Forms / Membership Provider app for testing the Puma Scan rules engine.

## Web Services

### Fox 
ASP.NET 4.6 Web API services project for testing the Puma Scan rules engine.

## Data Tier

### Rabbit
Database tier with ADO, LINQ, and EF context classes for querying data using the main .NET data access frameworks.
Mon Jan 25 15:55:53 CST 2021
Mon Jan 25 15:59:00 CST 2021
Mon Jan 25 16:01:43 CST 2021
Mon Jan 25 16:06:27 CST 2021
Mon Jan 25 16:37:41 CST 2021
Mon Jan 25 17:45:25 CST 2021
Mon Jan 25 17:45:32 CST 2021
Mon Jan 25 18:07:41 CST 2021
Mon Jan 25 18:39:04 CST 2021
Mon Jan 25 18:54:13 CST 2021
Wed Jan 27 09:05:01 CST 2021
Wed Jan 27 09:28:56 CST 2021
Wed Jan 27 09:38:12 CST 2021
Wed Jan 27 22:55:32 CST 2021

FROM debian:buster
MAINTAINER example@control-plane.io

RUN \
    DEBIAN_FRONTEND=noninteractive \
    apt-get update && apt-get install -y --no-install-recommends \
        bash \
    \
    && rm -rf /var/lib/apt/lists/*